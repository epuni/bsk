package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testFirstThrow() throws BowlingException {
		final var frame = new Frame(1,2);
		assertEquals(1, frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrow() throws BowlingException {
		final var frame = new Frame(1,2);
		assertEquals(2, frame.getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void testNegativeInitialization() throws BowlingException {
		new Frame(-1, 2);
	}
	
	@Test(expected = BowlingException.class)
	public void testNegativeInitialization2() throws BowlingException {
		new Frame(8, -2);
	}
	
	@Test(expected = BowlingException.class)
	public void testOverflowInitialization() throws BowlingException {
		new Frame(2, 80);
	}

	@Test
	public void testFrameScore() throws BowlingException {
		final var frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testFrameIsSpare() throws BowlingException {
		assertEquals(true, new Frame(2, 8).isSpare());
		assertEquals(false, new Frame(6, 1).isSpare());
		assertEquals(false, new Frame(10, 0).isSpare());
	}
	
	@Test
	public void equalityTest() throws BowlingException
	{
		var frame = new Frame(2, 8);
		frame.setBonus(2);
		
		var frame2 = new Frame(2, 8);
		assertNotEquals(frame, frame2);
		
		frame2.setBonus(2);
		assertEquals(frame, frame2);
	}
	
	@Test
	public void testFrameScoreBonus() throws BowlingException {
		// Spare bonus
		var frame = new Frame(2, 8);
		frame.setBonus(2);
		assertEquals(12, frame.getScore());
		
		// Strike bonus
		frame = new Frame(10, 0);
		frame.setBonus(3);
		assertEquals(13, frame.getScore());
	}
	
	@Test(expected = BowlingException.class)
	public void testFrameInvalidBonus() throws BowlingException {
		final var frame = new Frame(2, 7);
		frame.setBonus(2);
	}
	
	@Test
	public void testFrameIsStrike() throws BowlingException {
		assertEquals(true, new Frame(10, 0).isStrike());
		assertEquals(false, new Frame(0, 10).isStrike());
	}
}
