package tdd.training.bsk;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GameBonusTest {
	private void appendExtraFrames(Game game, boolean skipLast) throws BowlingException {
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		
		if (!skipLast)
			game.addFrame(new Frame(2, 6));
	}
	
	private void appendExtraFrames(Game game) throws BowlingException {
		appendExtraFrames(game, false);
	}
	
	@Test
	public void testSpareScore() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game);
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testStrikeScore() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game);
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testStrikeSpareScore() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		appendExtraFrames(game);
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testTwoStrikesScore() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		appendExtraFrames(game);
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testTwoSparesScore() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		appendExtraFrames(game);
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testSpareAsLastFrame() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game, true);
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testStrikeAsLastFrame() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game, true);
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(7, game.getFirstBonusThrow());
		assertEquals(2, game.getSecondBonusThrow());
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test(expected=BowlingException.class)
	public void testWrongSecondBonus() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game, true);
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
	}
	
	@Test(expected=BowlingException.class)
	public void testOverflowBonus() throws BowlingException {
		final var game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		appendExtraFrames(game, true);
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(11);
		
		game.calculateScore();
	}
	
	@Test
	public void testAllStrikesScore() throws BowlingException {
		final var game = new Game();
		for (int i = 0; i < 10; i++)
			game.addFrame(new Frame(10, 0));	
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
}
