package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	private Game game;
	
	@Before
	public void initializeGame() throws BowlingException
	{
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	}
	
	@Test
	public void testGetFrame() throws BowlingException {
		// First
		assertEquals(new Frame(1, 5), game.getFrameAt(0));
		// Middle
		assertEquals(new Frame(5, 3), game.getFrameAt(5));
		// Last
		assertEquals(new Frame(2, 6), game.getFrameAt(9));
	}	
	
	@Test(expected = BowlingException.class)
	public void testOutOfBoundsFrame() throws BowlingException {
		game.getFrameAt(10);
	}
	
	@Test(expected = BowlingException.class)
	public void testTooManyFrames() throws BowlingException {
		game.addFrame(new Frame(1,1));
	}
	
	@Test(expected = BowlingException.class)
	public void testNullFrame() throws BowlingException {
		game = new Game();
		game.addFrame(null);
	}
	
	@Test
	public void testCalculateScore() throws BowlingException {
		assertEquals(81, game.calculateScore());
	}
	
	@Test(expected = BowlingException.class)
	public void testCalculateScoreIncomplete() throws BowlingException {
		game = new Game();
		game.calculateScore();
	}
	
	@Test(expected = BowlingException.class)
	public void testWrongBonus() throws BowlingException {
		// Last is not a spare
		game.setFirstBonusThrow(1);
	}
	
	@Test(expected = BowlingException.class)
	public void testWrongBonus2() throws BowlingException {
		game = new Game();
		// The game is not complete
		game.setFirstBonusThrow(1);
	}
	
	@Test(expected = BowlingException.class)
	public void testWrongBonus3() throws BowlingException {
		// Last is not a strike
		game.setSecondBonusThrow(1);
	}
	
	@Test(expected = BowlingException.class)
	public void testWrongBonus4() throws BowlingException {
		game = new Game();
		// The game is not complete
		game.setSecondBonusThrow(1);
	}
}
