package tdd.training.bsk;

import java.util.stream.Stream;

public class Game {
	private static final int FRAME_MAX = 10;
	
	private final Frame[] frames = new Frame[FRAME_MAX];
	private int frameCount = 0;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// Nothing to do here
	}
	
	/**
	 * Checks whether there is the nth previous element
	 */
	private boolean hasLast(int count)
	{
		return frameCount >= count + 1;
	}
	
	/**
	 * Get the nth previous element
	 */
	private Frame getLast(int count)
	{
		return frames[frameCount - count - 1];
	}

	/**
	 * Checks whether all the frames have been set
	 */
	private boolean isGameComplete() {
		return frameCount == FRAME_MAX;
	}
	
	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (frame == null)
			throw new BowlingException("frame can't be null");
		
		if (isGameComplete())
			throw new BowlingException("This game already has all its frames set");
		
		if (hasLast(0))
		{
			final var previous = getLast(0);
			
			if (previous.isSpare()) 
				previous.setBonus(frame.getFirstThrow());
			else if (previous.isStrike()) {
				previous.setBonus(frame.getScore());
				
				if (hasLast(1) && getLast(1).isStrike())
				{
					final var prevStrike = getLast(1);
					prevStrike.setBonus(prevStrike.getBonus() + frame.getFirstThrow());
				}
			}
		}
			
		frames[frameCount++] = frame;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index < 0 || index >= frameCount)
			throw new BowlingException("Out of bounds frame accessed");
		
		if (frames[index] == null)
			throw new BowlingException("Assertion failed !");
		
		return frames[index];
	}
	
	/**
	 * Check whether the bonus can be added
	 */
	private void asserBonusValid(int first, int second) throws BowlingException {
		if (!isGameComplete())
			throw new BowlingException("Bonuses can only be assigned once all frames have been set");
		
		if (first < 0 || first > Frame.MAX_PINS || second > Frame.MAX_PINS)
			throw new BowlingException("Invalid throw points value");
		
		if (!getLast(0).allowsBonus() && first != 0)
			throw new BowlingException("The last frame doesn't allow for bonus throws");
		
		if (!getLast(0).isStrike() && second != 0)
			throw new BowlingException("The last frame doesn't allow for a second bonus throw");
	}
	
	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		asserBonusValid(firstBonusThrow, secondBonusThrow);
		this.firstBonusThrow = firstBonusThrow;
		
		if (getLast(0).isStrike() && getLast(1).isStrike())
			getLast(1).setBonus(getLast(1).getBonus() + firstBonusThrow);
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		asserBonusValid(firstBonusThrow, secondBonusThrow);
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}
	
	/**
	 * Returns the total bonus throws of this game.
	 */
	public int getTotalBonusScore() {
		return secondBonusThrow + firstBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		if (!isGameComplete())
			throw new BowlingException("Can't calculate the score for an incomplete game");
		
		return Stream.of(frames)
			.map(Frame::getScore)
			.reduce((x,y) -> x + y)
			.get() + getTotalBonusScore();
	}

}
