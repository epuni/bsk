package tdd.training.bsk;

import java.util.Objects;

public class Frame {

	public static final int MAX_PINS = 10;
	
	private final int firstThrow;
	private final int secondThrow;
	private int bonus;
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if (firstThrow < 0 || secondThrow < 0)
			throw new BowlingException("Negative throw values are not allowed");
		
		if (firstThrow + secondThrow > MAX_PINS)
			throw new BowlingException("Can't knock down more than " + MAX_PINS + " pins");
		
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) throws BowlingException{
		if (!isSpare() && !isStrike())
			throw new BowlingException("Can't set bonus points to a non-spare frame");
		
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return firstThrow + secondThrow + bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return firstThrow == MAX_PINS;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return !isStrike() && firstThrow + secondThrow == MAX_PINS;
	}
	
	/**
	 * It returns whether, or not, this frame gives a bonus.
	 */
	public boolean allowsBonus() {
		return isStrike() || isSpare();
	}

	// Auto generated
	@Override
	public int hashCode() {
		return Objects.hash(bonus, firstThrow, secondThrow);
	}

	// Auto generated
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Frame))
			return false;
		Frame other = (Frame) obj;
		return bonus == other.bonus && firstThrow == other.firstThrow && secondThrow == other.secondThrow;
	}

}
